using Godot;
using System;

public class CameraController : Camera
{
	// Declare member variables here.
	public float panSpeed = 25; //Meters a second
	public float panAccel = 1.5F; //Meters / second/second
	public float rotationSensitivity = 0.06F; //Degrees pixel of mouse movement

	private bool rotateMode = true;
	private Vector2 mouseDelta;

/*	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{

	}
*/
	//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		handleInput(delta);

		//If in rotate mode track the mouse
		if (rotateMode)
		{
			//Calculate Mouse Movement From Origin point

			RotationDegrees += new Vector3(-mouseDelta.y, -mouseDelta.x, 0) * rotationSensitivity;
		}

		mouseDelta = Vector2.Zero; //Reset the delta to zero so the mouse won't drift between input events.
	}

	public override void _Input(InputEvent @event)
	{
		if(@event is InputEventMouseMotion mouseMotion)
		{
			mouseDelta = mouseMotion.Relative;
		}
	}

	private void handleInput(float delta)
	{
		if (Input.IsActionPressed("pan_right"))
		{
			TranslateObjectLocal(new Vector3(panSpeed, 0, 0) * delta);
		}

		if (Input.IsActionPressed("pan_left"))
		{
			TranslateObjectLocal(new Vector3(-panSpeed, 0, 0) * delta);
		}

		if (Input.IsActionPressed("pan_forward"))
		{
			TranslateObjectLocal(new Vector3(0, 0, -panSpeed) * delta);
		}

		if (Input.IsActionPressed("pan_backward"))
		{
			TranslateObjectLocal(new Vector3(0, 0, panSpeed) * delta);
		}

		if (Input.IsActionPressed("pan_up"))
		{
			TranslateObjectLocal(new Vector3(0, panSpeed, 0) * delta);
		}

		if (Input.IsActionPressed("pan_down"))
		{
			TranslateObjectLocal(new Vector3(0, -panSpeed, 0) * delta);
		}

		if (Input.IsActionJustPressed("free_cursor"))
		{
			rotateMode = false;
			Input.SetMouseMode(Input.MouseMode.Visible);
		}

		if (Input.IsActionJustReleased("free_cursor"))
		{
			rotateMode = true;
			Input.SetMouseMode(Input.MouseMode.Captured);
		}

		if (Input.IsActionPressed("increase_pan_speed")){
			panSpeed += panAccel * delta;
			GD.Print("PanSpeed: " + panSpeed);
		}

		if (Input.IsActionPressed("decrease_pan_speed"))
		{
			panSpeed -= panAccel * delta;

			if(panSpeed < 1)
			{
				panSpeed = 1;
			}

			GD.Print("PanSpeed:" + panSpeed);
		}
	}
}
