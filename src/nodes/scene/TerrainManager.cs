using Godot;
using Nanomancer;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 /*
 * Terrain Manager Is Responsible for generating, releasing, and re-loading the terrain of the world around the player
 */
public class TerrainManager : Node
{
	//The object that is representing the player. Chunks will be unloaded and loaded around this object's position 
	[Export]
	public NodePath playerPath = new NodePath();
	
	public Spatial player;

	[Export]
	public int worldSeed = 12;

	//Simplex Parameters
	public int octaves = 3;
	public float persistence = 0.8F;

	//How Many Tiles to Each Side of a chunk
	public int chunkSize = 16;

	//How long each side of the tiles comprising a chunk are
	public float tileSize = 0.5F;
	/*
		* The distance in chunks that a chunk will render.
		* If a chunk is completely inside the radius defined by this parameter*tileSize it will generate and render
		* If a chunk is partially inside the radius defined by this parameter*tileSize, it will generate, but will not render until it's completely inside the radius
		*/
	public int renderDistance = 1;

	private OpenSimplexNoise noise;
	private List<ChunkVO> chunks = new List<ChunkVO>(); //Generally should be all chunks within or touching the radius
	private Vector3 lastPlayerPosition;
	private Vector3 playerPositionAtLastUpdate;

	private float lastRan;
	public TerrainManager(int worldSeed, int octaves, float persistence, int chunkSize, float tileSize)
	{

		this.worldSeed = worldSeed;
		this.octaves = octaves;
		this.persistence = persistence;
		this.chunkSize = chunkSize;
		this.tileSize = tileSize;

		this.noise = new OpenSimplexNoise();
		noise.Octaves = this.octaves;
		noise.Persistence = this.persistence;
		noise.Seed = this.worldSeed;
	}
		
	public TerrainManager() {
		this.noise = new OpenSimplexNoise();
		noise.Octaves = this.octaves;
		noise.Persistence = this.persistence;
		noise.Seed = this.worldSeed;
	}

	public override void _Ready()
	{
		player = GetNode<Spatial>(playerPath);
		lastPlayerPosition = player.GlobalTransform.origin;
		lastRan = 0;

		updateTerrain(); // Do once to get us started
	}

	//Really we should dispatch the heavy lifting to another thread and fetch / re-calculate around once a second only if the player moved
	public override void _Process(float delta)
	{
		lastRan += delta;

		if(lastRan < 1)
		{
			return;
		}

		if (lastPlayerPosition.Equals(player.GlobalTransform.origin)){
			return;//If we didn't move we don't need to recalculate.
		}

		if (playerPositionAtLastUpdate.DistanceTo(player.GlobalTransform.origin) < chunkSize * tileSize)
		{
			return;
		}

		lastPlayerPosition = player.GlobalTransform.origin;//Should only run if the position changes

		updateTerrain(); // Heavy Lifting
	}

	public void updateTerrain()
	{
		playerPositionAtLastUpdate = player.GlobalTransform.origin;
		lastRan = 0;
		GD.Print("Processing Terrain");
		// Drop any chunks from the last frame that are outside the radius completely\
		chunks = GetChildren().OfType<ChunkVO>().ToList(); //Get all chunks that are a child object;
		GD.Print("Chunk Count: " + chunks.Count);
		chunks
			.FindAll(chunk => isChunkInRadius(chunk) == RadiusRange.OUT)
			.ForEach(foundChunk =>
			{
				foundChunk.FreeMesh();
				RemoveChild(foundChunk);
				chunks.Remove(foundChunk);
				foundChunk.QueueFree();
			});



		//Calculate all chunk coords within the radius
		Vector2 playerChunkCoordinate = ChunkUtils.GetChunkCoordsForGlobalCoord(new Vector2(player.GlobalTransform.origin.x, player.GlobalTransform.origin.z), chunkSize, tileSize);

		for (int z = (int)Math.Floor(playerChunkCoordinate.y) - renderDistance; z < playerChunkCoordinate.y + renderDistance; z++)
		{
			for (int x = (int)Math.Floor(playerChunkCoordinate.x) - renderDistance; x < playerChunkCoordinate.x + renderDistance; x++)
			{
				//See if chunk is already in array, if it is, continue
				if (chunks.Find(chunk => chunk.chunkCoordinates.Equals(new Vector2(x, z))) != null)
				{
					continue;
				}

				//If we're here we need to create a new chunk and position it accordingly
				ChunkVO newChunk = CreateChunk(new Vector2(x, z));

				newChunk.Translate(new Vector3(x * chunkSize * tileSize, 0, z * chunkSize * tileSize));

				chunks.Add(newChunk);
				AddChild(newChunk);
			}
		}

	}
	public List<ChunkVO> getChunks()
	{
		return this.chunks;
	}
	/*
		* Generates a chunk with its info. Based off of the Chunk Coordinates
		*/
	public ChunkVO CreateChunk(Vector2 chunkCoords)
	{
		ChunkVO chunk = new ChunkVO();
		chunk.chunkCoordinates = chunkCoords;
		chunk.tileSize = tileSize;
		chunk.length = chunkSize;

		//Determine the offset for the constructed Mesh instance
		Vector2 offset = new Vector2(chunkCoords.x * tileSize * chunkSize, chunkCoords.y * tileSize * chunkSize);
		MeshTools mt = new MeshTools();

		MeshInstance meshInstance = new MeshInstance();
		meshInstance.Mesh = mt.GeneratePerlinMesh(chunk.length, chunk.length, chunk.tileSize, offset, noise);
		meshInstance.CreateTrimeshCollision();

		chunk.SetMesh(meshInstance);
		return chunk;
	}

	private RadiusRange isChunkInRadius(ChunkVO chunk)
	{
		Vector2 tl, bl, tr, br;
		tl = chunk.chunkCoordinates * chunkSize * tileSize;
		bl = new Vector2(tl.x, tl.y + chunkSize * tileSize);
		tr = new Vector2(tl.x + chunkSize * tileSize, tl.y);
		br = new Vector2(tl.x + chunkSize * tileSize, tl.y + chunkSize * tileSize);

		bool oneInside = false;
		bool allInside = false;

		if (isPointInsideRadius(bl) || isPointInsideRadius(tl) || isPointInsideRadius(tr) || isPointInsideRadius(br))
		{
			oneInside = true;
		} else if (isPointInsideRadius(bl) && isPointInsideRadius(tl) && isPointInsideRadius(tr) && isPointInsideRadius(br))
		{
			allInside = true;
		}

		if (allInside)
		{
			return RadiusRange.IN;
		}else if (oneInside)
		{
			return RadiusRange.PARTIAL;
		}
		else
		{
			return RadiusRange.OUT;
		}

	}

	private bool isPointInsideRadius(Vector2 point)
	{
		Vector3 playerPos = player.GlobalTransform.origin;
		return (Math.Pow(point.x - playerPos.x, 2) + Math.Pow(point.y - playerPos.z, 2)) <= Math.Pow((renderDistance * tileSize * chunkSize), 2);

	}

}


