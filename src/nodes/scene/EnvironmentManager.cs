﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*
 * Environment manager is responsible for the terrain, items, and other elements of the setting around the player
 * 
 * 
 */
public class EnvironmentManager : Node
{ 
    public Spatial player;
    public TerrainManager tMgr;
    
    public EnvironmentManager() { }
    public EnvironmentManager(Spatial player)
    {

        this.tMgr =  new TerrainManager((int)new RandomNumberGenerator().Randi(),
                                        3, //Octaves
                                        0.3F, //Persistence
                                        8, //Chunk Size
                                        1); //Tile Size --Probably Externalize These
    }


}

