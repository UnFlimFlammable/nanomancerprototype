using Godot;
using System;
using System.Collections;
using System.Collections.Generic;

	public class MeshTools : MeshInstance
	{
		SurfaceTool sf = new SurfaceTool();


		public Mesh GenerateTriangleMesh()
		{
			sf.Begin(Mesh.PrimitiveType.Triangles);

			sf.AddColor(new Color(1, 0, 0, 0));
			sf.AddVertex(new Vector3(0, 0, 0));

			sf.AddColor(new Color(1, 0, 0, 0));
			sf.AddVertex(new Vector3(1, 0, 0));

			sf.AddColor(new Color(1, 0, 0, 0));
			sf.AddVertex(new Vector3(1, 0, 1));


			sf.AddColor(new Color(1, 0, 0, 0));
			sf.AddVertex(new Vector3(1, 0, 1));

			sf.AddColor(new Color(1, 0, 0, 0));
			sf.AddVertex(new Vector3(2, 0, 2));


			sf.AddColor(new Color(2, 0, 0, 0));
			sf.AddVertex(new Vector3(2, 0, 2));

			sf.GenerateNormals();

			return sf.Commit();
		}

		public Mesh GeneratePlanarMesh(int width, int height, float tileSize)
		{
			//Generate the Vertices (2 tris per tile, 3 vertices per tri)
			List<Vector3> vertices = GeneratePlanarVertices(width, height, tileSize);

			return GenerateTriMeshFromVertices(vertices);
		}

		public Mesh GeneratePerlinMesh(int width, int height, float tileSize, Vector2 sampleOffset, OpenSimplexNoise noise)
		{
			List<Vector3> vertices = GeneratePlanarVertices(width, height, tileSize);

			//Apply noise to the vertices
			int x, z = 0;
			for (int i = 0; i < vertices.Count; i++)
			{

				if (z != 0 && i % z == 0) //If its a multiple of a row, increment the row counter
				{
					z++;
				}

				if (z != 0)
				{
					x = i % z;
				}
				else
				{
					x = i;
				}


				Vector3 vert = vertices[i];

				//Sample the noise to determine the y value of the vertice with an offset 
				vert.y = noise.GetNoise2d(vertices[i].x + sampleOffset.x, vertices[i].z + sampleOffset.y) * 15;

				vertices[i] = vert; //Probably unnecessary? 
			}

			MeshInstance mi = new MeshInstance();
			return GenerateTriMeshFromVertices(vertices);
		}

		private Mesh GenerateTriMeshFromVertices(List<Vector3> vertices)
		{
			sf.Begin(Mesh.PrimitiveType.Triangles);
			foreach (Vector3 vertice in vertices)
			{
				sf.AddColor(Colors.Black);
				sf.AddVertex(vertice);
			}

			sf.GenerateNormals();
			sf.SetMaterial((Material)GD.Load("res://RootScene.tres"));
			return sf.Commit();
		}

		public List<Vector3> GeneratePlanarVertices(int width, int height, float tileSize)
		{
			//Generate the Vertices (2 tris per tile, 3 vertices per tri)
			List<Vector3> vertices = new List<Vector3>();

			for (int z = 0; z < height; z++)
			{
				for (int x = 0; x < width; x++)
				{
					//Push the vertices that form the tile here
					Vector3 topLeft = new Vector3(x * tileSize, 0, z * tileSize);
					Vector3 topRight = new Vector3(x * tileSize + tileSize, 0, z * tileSize);
					Vector3 bottomRight = new Vector3(x * tileSize + tileSize, 0, z * tileSize + tileSize);
					Vector3 bottomLeft = new Vector3(x * tileSize, 0, z * tileSize + tileSize);

					//Push the Top Left Triangle with Vertices going clockwise
					vertices.Add(new Vector3(topLeft));
					vertices.Add(new Vector3(topRight));
					vertices.Add(new Vector3(bottomLeft));

					//Push the bottom right triangle with vertices going clockwise
					vertices.Add(new Vector3(bottomLeft));
					vertices.Add(new Vector3(topRight));
					vertices.Add(new Vector3(bottomRight));
				}
			}

			return vertices;
		}

	}


