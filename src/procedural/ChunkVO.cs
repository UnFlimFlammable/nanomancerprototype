﻿using Godot;
using System;

/*
 * The Chunk class is a 'chunk' of terrain of varying size. In the future, it may benefit memory usage to remove the tileSize and length variables as I can't imagine a map with multiple chunks
 */
public class ChunkVO : Spatial
{
    public void SetMesh(MeshInstance mesh)
    {
        this.mesh = mesh;
        AddChild(this.mesh);
    }

    public void FreeMesh()
    {
        this.RemoveChild(this.mesh);
        mesh.QueueFree();
    }


    public float tileSize = .5F; // Size of each tile
    public int length = 16; // Length in tiles
    public Vector2 chunkCoordinates; //The position of this chunk on the chunk grid
    public MeshInstance mesh; // The actual mesh object
}

