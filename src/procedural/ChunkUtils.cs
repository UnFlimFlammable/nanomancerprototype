﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ChunkUtils
{
    public int chunkLength; // Length in Tiles
    public float tileSize; // Tile Size in global units

    public ChunkUtils(int chunkLength, float tileSize)
    {
        if (chunkLength <= 0 || tileSize <= 0)
        {
            throw new ArgumentOutOfRangeException("Either ChunkLength or Tile Size is less than or equal to zero");
        }

        this.chunkLength = chunkLength;
        this.tileSize = tileSize;
    }

    public static Vector2 GetChunkCoordsForGlobalCoord(Vector2 globalCoords, int chunkLength, float tileSize)
    {
        if (chunkLength <= 0 || tileSize <= 0)
        {
            throw new ArgumentOutOfRangeException("Either ChunkLength or Tile Size is less than or equal to zero");
        }

        return new Vector2((float) Math.Floor(globalCoords.x / (chunkLength * tileSize)), (float) Math.Floor(globalCoords.y / (chunkLength * tileSize)));
    }

    public Vector2 GetChunkCoordsForGlobalCoord(Vector2 globalCoords)
    {
        if(chunkLength <= 0 || tileSize <= 0)
        {
            throw new ArgumentOutOfRangeException("Either ChunkLength or Tile Size is less than or equal to zero");
        }

        return new Vector2((float) Math.Floor(globalCoords.x / (chunkLength * tileSize)), (float) Math.Floor(globalCoords.y / (chunkLength * tileSize)));
    }
    
}

